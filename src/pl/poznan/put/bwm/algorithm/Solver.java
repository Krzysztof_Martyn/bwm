package pl.poznan.put.bwm.algorithm;

import java.util.ArrayList;
import java.util.List;

import org.gnu.glpk.GLPK;
import org.gnu.glpk.GLPKConstants;
import org.gnu.glpk.SWIGTYPE_p_double;
import org.gnu.glpk.SWIGTYPE_p_int;
import org.gnu.glpk.glp_iocp;
import org.gnu.glpk.glp_prob;
import org.gnu.glpk.glp_smcp;

public class Solver {
	private glp_prob prob;
	private List<Integer> weights_ids;
	private Integer ksi;
	private int criterion_count;
	
	public Solver(int criterion_count) {
		this.criterion_count = criterion_count;
		initProblem();
		this.weights_ids = generateVariables(this.criterion_count, "weight");
		this.ksi = generateVariableLB("ksi");
		

	}

	private void initProblem() {
		prob = GLPK.glp_create_prob();
		GLPK.glp_set_prob_name(prob, "bwmProblem");
	}
	
	private Integer generateVariable(String title) {		
		int variable = GLPK.glp_add_cols(prob, 1);            
		String name = String.format("variable_{0}", title);
        GLPK.glp_set_col_name(prob, variable, name);            
        GLPK.glp_set_col_kind(prob, variable, GLPK.GLP_CV);
        GLPK.glp_set_col_bnds(prob, variable, GLPK.GLP_DB, 0.0, 1.0);
        return variable;
	}
	private Integer generateVariableLB(String title) {		
		int variable = GLPK.glp_add_cols(prob, 1);            
		String name = String.format("variable_{0}", title);
        GLPK.glp_set_col_name(prob, variable, name);            
        GLPK.glp_set_col_kind(prob, variable, GLPK.GLP_CV);
        GLPK.glp_set_col_bnds(prob, variable, GLPK.GLP_LO, 0.0, 0.0);
        return variable;
	}
	
	private  List<Integer> generateVariables(int count, String title)
    {
        List<Integer> ids = new ArrayList<Integer>();        
        for (int i = 0; i < count; i++)
        {                
            ids.add(generateVariable(String.format("variable_{0}_{1}", title, i)));
        }
        return ids;
    }

	private void addConstraintsBestToOthers(List<Double> best_to_others, int best_id){
		for(int i=0; i<this.criterion_count;i++) {
			if(i!=best_id) {
				double weight = best_to_others.get(i);
				addConstraint(weights_ids.get(best_id),weights_ids.get(i),weight,"best_to_others_"+i);
			}
		}
	}
	private void addConstraintsOthersToWorst(List<Double> others_to_worst, int worst_id){
		for(int i=0; i<this.criterion_count;i++) {
			if(i!=worst_id) {
				double weight = others_to_worst.get(i);
				addConstraint(weights_ids.get(i),weights_ids.get(worst_id),weight,"others_to_worst_"+i);
			}
		}
	}
	
	private void addConstraint(int id_first, int id_second, double weight, String title) {
		addConstraint1(id_first, id_second, weight, title);
		addConstraint2(id_first, id_second, weight, title);
	}
	private void addConstraint1(int id_first, int id_second, double weight, String title) {
		int row;
        SWIGTYPE_p_int ind = GLPK.new_intArray(1 + 3);
        SWIGTYPE_p_double val = GLPK.new_doubleArray(1 + 3);
        row = GLPK.glp_add_rows(prob, 1);
        GLPK.glp_set_row_bnds(prob, row, GLPK.GLP_UP, 0, 0);
        GLPK.glp_set_row_name(prob, row, "equations_Constraint_1_"+title);
        GLPK.intArray_setitem(ind, 1, id_first);
        GLPK.doubleArray_setitem(val, 1, 1.0);
        GLPK.intArray_setitem(ind, 2, id_second);
        GLPK.doubleArray_setitem(val, 2, -weight);
        GLPK.intArray_setitem(ind, 3, this.ksi);     
        GLPK.doubleArray_setitem(val, 3, -1.0);
        GLPK.glp_set_mat_row(prob, row, 3, ind, val);
        GLPK.delete_intArray(ind);
        GLPK.delete_doubleArray(val);   

	}
	private void addConstraint2(int id_first, int id_second, double weight, String title) {
		int row;
        SWIGTYPE_p_int ind = GLPK.new_intArray(1 + 3);
        SWIGTYPE_p_double val = GLPK.new_doubleArray(1 + 3);
        row = GLPK.glp_add_rows(prob, 1);
        GLPK.glp_set_row_bnds(prob, row, GLPK.GLP_LO, 0, 0);
        GLPK.glp_set_row_name(prob, row, "equations_Constraint_2_"+title);
        GLPK.intArray_setitem(ind, 1, id_first);
        GLPK.intArray_setitem(ind, 2, id_second);
        GLPK.intArray_setitem(ind, 3, this.ksi);
        GLPK.doubleArray_setitem(val, 1, 1.0);
        GLPK.doubleArray_setitem(val, 2, -weight);
        GLPK.doubleArray_setitem(val, 3, 1.0);
        GLPK.glp_set_mat_row(prob, row, 3, ind, val);
        GLPK.delete_intArray(ind);
        GLPK.delete_doubleArray(val);    

	}
	
	private void addGoal() {
		GLPK.glp_set_obj_name(prob, "ksi");
		GLPK.glp_set_obj_dir(prob, GLPKConstants.GLP_MIN);
		GLPK.glp_set_obj_coef(prob, this.ksi, 1.);

	}
	private void addWeightNormalizationConstraint() {
        int row;
        SWIGTYPE_p_int ind = GLPK.new_intArray(1 + this.criterion_count);
        SWIGTYPE_p_double val = GLPK.new_doubleArray(1 + this.criterion_count);
        row = GLPK.glp_add_rows(prob, 1);
        GLPK.glp_set_row_bnds(prob, row, GLPK.GLP_FX, 1, 1);
        GLPK.glp_set_row_name(prob, row, "equations_SumBinaryLessOne");

        for (int p = 0; p < this.criterion_count; p++)
        {
            GLPK.intArray_setitem(ind, p+1, this.weights_ids.get(p));
            GLPK.doubleArray_setitem(val, p+1, 1.0);
        }
        GLPK.glp_set_mat_row(prob, row, this.criterion_count, ind, val);
        GLPK.delete_intArray(ind);
        GLPK.delete_doubleArray(val);
	}

	public boolean run(List<Double> best_to_others, List<Double> others_to_worst, int best_id, int worst_id) {
		addWeightNormalizationConstraint();
		addConstraintsBestToOthers(best_to_others,best_id);
		addConstraintsOthersToWorst(others_to_worst,worst_id);
		addGoal();	
		return solve();
	}
	
	private boolean solve() {			
		GLPK.glp_adv_basis(prob, 0);
        glp_smcp parm = new glp_smcp();
        GLPK.glp_init_smcp(parm);
        
        
        GLPK.glp_simplex(prob, parm);
        //GLPK.glp_print_ranges(prob, 0, null, 0, "result2.txt");

        return GLPK.glp_get_status(prob) == GLPK.GLP_OPT;
	}
	public List<Double> getResult(){
		List<Double> result = new ArrayList<Double>();
		for(int ids : weights_ids) {
			result.add(GLPK.glp_get_col_prim(prob, ids));
		}
		return result;
		
	}
    public double getOptymalizationValue()
    {
        return GLPK.glp_get_obj_val(prob);
    }
    public void delete() {
    	GLPK.glp_delete_prob(prob);

    }
}
